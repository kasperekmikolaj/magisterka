import os
import cv2
import numpy as np
from PIL import ImageOps, Image, ImageEnhance
from colormath.color_conversions import convert_color
from colormath.color_diff import delta_e_cie2000
from colormath.color_objects import sRGBColor, LabColor
from matplotlib import pyplot as plt

from solo_tries.tresholding import thresholdImage, deleteNotConnectedAndFillInside, smoothEdges

cellsDistance = 100
brightnessFactor = 1.5
minRowHighToDeleteColumn = 3

borderLen = 70
borderColor = (238, 233, 204)

imageHeight = 100
imageWidth = imageHeight

redColor = (255, 0, 0)
blackColor = 0
whiteColor = 255


def adjustCropIndexesWithBorder(middleX, middleY):
    left = int(middleX - imageWidth / 2 + borderLen)
    right = int(middleX + imageWidth / 2 + borderLen)
    top = int(middleY - imageHeight / 2 + borderLen)
    bot = int(middleY + imageHeight / 2 + borderLen)
    return left, top, right, bot


def adjustCropIndexes(middleX, middleY):
    # 32 coz of library function shift
    left = middleX - imageWidth / 2 + 32
    right = middleX + imageWidth / 2 + 32
    top = middleY - imageHeight / 2 + 32
    bot = middleY + imageHeight / 2 + 32
    return left, top, right, bot


def saveImage(img, path, filename):
    exist = os.path.exists(path)
    if not exist:
        os.makedirs(path)
    img.save(path + filename)


def saveBinaryImage(img, path, filename):
    exist = os.path.exists(path)
    if not exist:
        os.makedirs(path)
    plt.imsave(path + filename, img)


def makeRedMid(image):
    image = makeRedAtPosition(image, int(image.shape[0] / 2), int(image.shape[1] / 2))
    return image


def makeRedAtPosition(image, x, y, color=[255, 0, 0]):
    image[x][y] = color
    image[x][y - 1] = color
    image[x][y + 1] = color
    image[x + 1][y] = color
    image[x + 1][y + 1] = color
    image[x + 1][y - 1] = color
    image[x - 1][y] = color
    image[x - 1][y + 1] = color
    image[x - 1][y - 1] = color
    return image


def findCellCenter(colorImage):
    brightness = ImageEnhance.Brightness(Image.fromarray(colorImage))
    otsuBright = thresholdImage(cv2.cvtColor(np.array(brightness.enhance(brightnessFactor)), cv2.COLOR_BGR2GRAY))
    oneCellImage = deleteNotConnectedAndFillInside(otsuBright)
    if oneCellImage is None:
        return None, None
    finalShape = smoothEdges(oneCellImage)
    firstBlackIndexX = None
    lastBlackIndexX = None
    firstBlackIndexY = None
    lastBlackIndexY = None
    for rowIndex in range(finalShape.shape[0]):
        if firstBlackIndexX is None:
            if blackColor in finalShape[rowIndex]:
                firstBlackIndexX = rowIndex
        else:
            if blackColor not in finalShape[rowIndex]:
                lastBlackIndexX = rowIndex - 1
                break
    for columnIndex in range(finalShape.shape[1]):
        if firstBlackIndexY is None:
            if blackColor in finalShape[:, columnIndex]:
                firstBlackIndexY = columnIndex
        else:
            if blackColor not in finalShape[:, columnIndex]:
                lastBlackIndexY = columnIndex - 1
                break
    if lastBlackIndexX is None:
        lastBlackIndexX = finalShape.shape[0] - 1
    if lastBlackIndexY is None:
        lastBlackIndexY = finalShape.shape[1] - 1

    try:
        midX = int((firstBlackIndexX + lastBlackIndexX) / 2)
        midY = int((firstBlackIndexY + lastBlackIndexY) / 2)
        return int(midX), int(midY)
    except Exception:
        return None, None


def imageFromBaseAndOtsu(baseImage: np.array, otsuImage: np.array, valToColor=255):
    colorCopy = baseImage.copy()
    isAnythingToColor = False
    for row_index in range(len(otsuImage)):
        for column_index in range(len(otsuImage[0])):
            if otsuImage[row_index][column_index] == valToColor:
                colorCopy[row_index][column_index] = [255, 255, 255]
    return colorCopy


def openImageWithBorder(imagePath):
    return ImageOps.expand(Image.open(imagePath), borderLen, borderColor)


def findLines(row):
    lines = []
    lastRowIndex = len(row) - 1
    startIndex = None
    for i in range(len(row)):
        if startIndex is None:
            if row[i] == blackColor:
                # skip last in row and single pixel noise
                if not i == lastRowIndex and not row[i + 1] == whiteColor:
                    startIndex = i
        else:
            if not i == lastRowIndex:
                if row[i] == whiteColor:
                    lines.append((startIndex, i - 1))
                    startIndex = None
            else:
                if row[i] == blackColor:
                    lines.append((startIndex, i))
    return lines


def deleteLine(row, line):
    for i in range(line[0], line[1] + 1):
        row[i] = whiteColor


def getMinHighToDeleteColumn(index, mid, imageLen):
    # 60 / 90 / 100
    safeSpotLen = 4
    closeFactor = 0.3
    midFactor = 0.45

    safeSpotStart = mid - safeSpotLen
    safeSpotEnd = mid + safeSpotLen
    closeStart = int(mid - imageLen * closeFactor)
    closeEnd = int(mid + imageLen * closeFactor)
    midStart = int(mid - imageLen * midFactor)
    midEnd = int(mid + imageLen * midFactor)

    if safeSpotStart <= index <= safeSpotEnd:
        return 15
    elif closeStart <= index <= closeEnd:
        return 3
    elif midStart <= index <= midEnd:
        return 2
    else:
        return 1


def deleteExtraLinesByRows(otsuImage, midX, midY):
    linesToDelete = []
    allLines = []
    for rowIndex in range(len(otsuImage[0])):
        lines = findLines(otsuImage[rowIndex])
        allLines.append(lines)
        if len(lines) > 1:
            minDistances = []
            for line in lines:
                if line[0] <= midY <= line[1]:
                    minDistances.append(0)
                else:
                    minDistances.append(min(abs(midY - line[0]), abs(midY - line[1])))
            currentMin = minDistances[0]
            currentMinIndex = 0
            for i in range(1, len(minDistances)):
                if minDistances[i] < currentMin:
                    linesToDelete.append((lines[currentMinIndex], rowIndex))
                    deleteLine(otsuImage[rowIndex], lines[currentMinIndex])
                    currentMin = minDistances[i]
                    currentMinIndex = i
                else:
                    linesToDelete.append((lines[i], rowIndex))
                    deleteLine(otsuImage[rowIndex], lines[i])


def getCellExtremeRowPointsFromMid(cellImage: Image):
    imageAsArray = np.asarray(cellImage)
    whiteColor = (255, 255, 255)
    startRow = None
    endRow = None

    midRowIndex = int(imageAsArray.shape[0] / 2)
    for rowIndex in range(midRowIndex, 0, -1):
        if (imageAsArray[rowIndex] == whiteColor).all():
            startRow = rowIndex + 1
            break

    for rowIndex in range(midRowIndex, imageAsArray.shape[0], 1):
        if (imageAsArray[rowIndex] == whiteColor).all():
            endRow = rowIndex - 1
            break

    if startRow is None:
        startRow = 0
    if endRow is None:
        endRow = imageAsArray.shape[0] - 1

    return startRow, endRow


def getCellExtremeColPointsFromMid(cellImage: Image):
    imageAsArray = np.asarray(cellImage)
    whiteColor = (255, 255, 255)
    startCol = None
    endCol = None

    midColIndex = int(imageAsArray.shape[1] / 2)
    for colIndex in range(midColIndex, 0, -1):
        if (imageAsArray[:, colIndex] == whiteColor).all():
            startCol = colIndex + 1
            break

    for colIndex in range(midColIndex, imageAsArray.shape[1], 1):
        if (imageAsArray[:, colIndex] == whiteColor).all():
            endCol = colIndex - 1
            break

    if startCol is None:
        startCol = 0
    if endCol is None:
        endCol = imageAsArray.shape[0] - 1

    return startCol, endCol


def extendImageSize(image, newSize, extensionColor=(255, 255, 255), pasteInMiddle=True):
    old_size = image.size
    new_im = Image.new("RGB", newSize, color=extensionColor)
    if pasteInMiddle:
        new_im.paste(image, ((newSize[0] - old_size[0]) // 2,
                             (newSize[1] - old_size[1]) // 2))
    else:
        new_im.paste(image)
    return new_im


clusterColors = [
    [205, 194, 176],
    [198, 198, 190],
    [239, 233, 237],
    [205, 186, 159],
]


def closest_background(inputColor):
    color1_rgb = sRGBColor(inputColor[0], inputColor[1], inputColor[2])
    color1_lab = convert_color(color1_rgb, LabColor)
    color_diffs = []
    for index, color in enumerate(clusterColors):
        color2_rgb = sRGBColor(color[0], color[1], color[2])
        color2_lab = convert_color(color2_rgb, LabColor)
        color_diff = delta_e_cie2000(color1_lab, color2_lab)
        color_diffs.append((color_diff, index))
    return min(color_diffs)[1]
