import os

from PIL import Image


def generateBackground(color):
    width = 160
    height = width

    img = Image.new(mode="RGB", size=(width, height), color=color)
    return img


if __name__ == "__main__":
    backgroundColors = [
        (240, 240, 216),
        (231, 224, 194),
        (236, 236, 211),
        (225, 229, 212),
        (230, 233, 223),
        (248, 246, 224),
        (249, 245, 210),
        (248, 249, 231),
        (233, 225, 188),
        (230, 221, 189),
        (234, 226, 186),
        (233, 235, 211),
    ]

    for index, color in enumerate(backgroundColors):
        img = generateBackground(color)
        path = "backgrounds_generated/"
        exist = os.path.exists(path)
        if not exist:
            os.makedirs(path)
        img.save(path + str(index) + ".png")
