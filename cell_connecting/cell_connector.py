import os
import random

import cv2
import numpy as np
from PIL import Image, ImageOps
from matplotlib import pyplot as plt

from Utils import extendImageSize


def pasteCellIntoBackground(imageToBlend: Image, background: Image, offset: (int, int) = (0, 0)):
    imageToBlend = np.asarray(imageToBlend)
    background = np.asarray(background)

    for rowIndex in range(imageToBlend.shape[0]):
        for colIndex in range(imageToBlend.shape[1]):
            if (imageToBlend[rowIndex][colIndex] != [255, 255, 255]).all():
                background[rowIndex + offset[0]][colIndex + offset[1]] = imageToBlend[rowIndex][colIndex]

    return Image.fromarray(background)


def findRowWithSmallestDistance(image: Image):
    whiteColor = (255, 255, 255)
    imgArray = np.asarray(image)
    smallestDistanceRowIndex = None
    smallestDistance = imgArray.shape[0]
    colBaseOffset = None

    startIndex = int(imgArray.shape[1] / 2)
    for rowIndex, row in enumerate(imgArray):
        firstRightIndex = -1
        for colIndex in range(startIndex, len(row)):
            if (row[colIndex] != whiteColor).any():
                firstRightIndex = colIndex
                break

        if firstRightIndex == -1:
            continue

        lastLeftIndex = -1
        for colIndex in range(startIndex, 0, -1):
            if (row[colIndex] != whiteColor).any():
                lastLeftIndex = colIndex
                break

        if lastLeftIndex == -1:
            continue

        rowDistance = firstRightIndex - lastLeftIndex
        if rowDistance < smallestDistance:
            smallestDistanceRowIndex = rowIndex
            smallestDistance = rowDistance
            colBaseOffset = firstRightIndex - lastLeftIndex

    return smallestDistanceRowIndex, colBaseOffset


def imageExtremePoints(image: Image):
    imageAsArray = np.asarray(image)
    whiteColor = (255, 255, 255)
    top = None
    bot = None

    for rowIndex in range(imageAsArray.shape[0]):
        if top is None:
            if (imageAsArray[rowIndex] != whiteColor).any():
                top = rowIndex
        else:
            if (imageAsArray[rowIndex] == whiteColor).all():
                bot = rowIndex - 1
                break
    if bot is None:
        bot = imageAsArray.shape[0] - 1

    left = None
    right = None
    for columnIndex in range(imageAsArray.shape[1]):
        if left is None:
            if (imageAsArray[:, columnIndex] != whiteColor).any():
                left = columnIndex
        else:
            if (imageAsArray[:, columnIndex] == whiteColor).all():
                right = columnIndex - 1
                break
    if right is None:
        right = imageAsArray.shape[1] - 1

    return left, top, right, bot


def connect(firstImagePath, secondImagePath, backgroundsPath, thirdImagePath=None):
    first_second_colOffset = random.randrange(8, 12)
    second_third_colOffset = random.randrange(8, 12)

    double_connection_size = 120
    triple_connection_size = 160

    workSizeForDouble = 210
    workSizeForTriple = 310
    extension = ".png"
    whiteColor = (255, 255, 255)

    # 0/90/180/270
    firstImageRotation = random.randrange(4) * 90
    secondImageRotation = random.randrange(4) * 90

    # load and rotate
    firstImage = Image.open(firstImagePath).rotate(firstImageRotation)
    secondImage = Image.open(secondImagePath).rotate(secondImageRotation)

    # create clear board
    white_board = Image.new("RGB", (workSizeForDouble, workSizeForDouble), color=whiteColor)

    # paste 1st into board
    firstPasted = pasteCellIntoBackground(firstImage, white_board)

    # generate second cell distance from top of the image
    rowOffsetBoundary = int(firstImage.height / 2) - 5
    rowOffsetForTwo = random.randrange(rowOffsetBoundary)

    # mirror and paste second
    mirror = ImageOps.mirror(firstPasted)
    secondPasted = pasteCellIntoBackground(secondImage, mirror, (rowOffsetForTwo, 0))

    # find connecting row index
    smallestRowDistanceIndex, colBaseOffsetForTwo = findRowWithSmallestDistance(secondPasted)

    if colBaseOffsetForTwo is None:
        return None

    # column offset
    finalColOffsetForTwo = (colBaseOffsetForTwo + first_second_colOffset)
    maxColOffsetForTwo = (firstPasted.width - int(firstImage.width / 2)) - 2
    finalColOffsetForTwo = finalColOffsetForTwo if finalColOffsetForTwo < maxColOffsetForTwo else maxColOffsetForTwo

    # final paste
    finalPaste = pasteCellIntoBackground(secondImage, ImageOps.mirror(firstPasted),
                                         (rowOffsetForTwo, finalColOffsetForTwo),
                                         # backgroundAlphaFactor
                                         )

    # crop
    rightCornerCropped = finalPaste.crop(imageExtremePoints(finalPaste))
    rightCornerCroppedHeight = rightCornerCropped.height
    rightCornerCroppedWidth = rightCornerCropped.width

    # extend to save-dimensions
    twoExtendedToSaveDimension = extendImageSize(rightCornerCropped, (double_connection_size, double_connection_size))

    # add background
    backNumber = random.randrange(len(os.listdir(backgroundsPath)))
    backgroundPath = backgroundsPath + str(backNumber) + extension
    backgroundImage = Image.open(backgroundPath)
    readyToSaveTwo = pasteCellIntoBackground(twoExtendedToSaveDimension, backgroundImage)

    plt.imshow(firstImage)
    plt.title("firstImage")
    plt.show()

    plt.imshow(secondImage)
    plt.title("secondImage")
    plt.show()

    plt.imshow(firstPasted)
    plt.title("firstPasted")
    plt.show()

    plt.imshow(mirror)
    plt.title("mirror")
    plt.show()


    secondPastedForDistanceCalc = np.asarray(secondPasted)
    secondPastedForDistanceCalc[smallestRowDistanceIndex] = np.asarray([[255, 0, 0] for i in range(len(np.asarray(secondPastedForDistanceCalc)))])

    plt.imshow(secondPastedForDistanceCalc)
    plt.title("secondPasted to find row with smallest distance")
    plt.show()

    plt.imshow(finalPaste)
    plt.title("finalPaste")
    plt.show()

    plt.imshow(rightCornerCropped)
    plt.title("cropped")
    plt.show()

    plt.imshow(readyToSaveTwo)
    plt.title("with background")
    plt.show()

    if thirdImagePath is None:
        blured = cv2.blur(np.asarray(readyToSaveTwo), (3, 3))

        plt.imshow(blured)
        plt.title("blured")
        plt.show()

        return blured

    # @@@@@@@@@@@@@@@@@@@@@@@@@@ 3rd image @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

    # rotate
    rightCornerCroppedRotation = random.randrange(4) * 90
    biggerDimension = rightCornerCroppedHeight if rightCornerCroppedHeight > rightCornerCroppedWidth else rightCornerCroppedWidth
    if rightCornerCroppedRotation == 1 or rightCornerCroppedRotation == 3:
        rightCornerCroppedHeight = rightCornerCroppedWidth
    extendedForRotation = extendImageSize(rightCornerCropped, (biggerDimension, biggerDimension),
                                          pasteInMiddle=False)
    rotatedThree = extendedForRotation.rotate(rightCornerCroppedRotation)

    # extend
    rightCornerExtended = extendImageSize(rotatedThree, (workSizeForTriple, workSizeForTriple),
                                          pasteInMiddle=False)

    # mirror
    rcMirror = ImageOps.mirror(rightCornerExtended)

    # prepare third
    thirdImageRotation = random.randrange(4) * 90
    thirdImage = Image.open(thirdImagePath).rotate(thirdImageRotation)

    # row offset
    thirdExtremes = imageExtremePoints(thirdImage)
    thirdCropped = thirdImage.crop(thirdExtremes)
    thirdCellRowOffset = random.randrange(rightCornerCroppedHeight - 3)
    thirdPastedForDistanceCalc = pasteCellIntoBackground(thirdCropped, rcMirror, (thirdCellRowOffset, 0))

    # find smallest distance row index
    smallestRowDistanceIndexForThree, colBaseOffsetForThree = findRowWithSmallestDistance(thirdPastedForDistanceCalc)

    if colBaseOffsetForThree is None:
        return None

    # column offset
    finalColOffsetForThree = (colBaseOffsetForThree + second_third_colOffset)
    maxColOffsetForThree = (workSizeForTriple - int(thirdImage.width / 2)) - 2
    finalColOffsetForThree = finalColOffsetForThree if finalColOffsetForThree < maxColOffsetForThree else maxColOffsetForThree

    # third paste
    thirdPastedFinal = pasteCellIntoBackground(thirdCropped, rcMirror,
                                               (thirdCellRowOffset, finalColOffsetForThree),
                                               # backgroundAlphaFactor
                                               )

    # crop
    rightCornerCroppedWithThreeCells = thirdPastedFinal.crop(imageExtremePoints(thirdPastedFinal))

    # extend to save dimensions
    threeExtendedToSaveDim = extendImageSize(rightCornerCroppedWithThreeCells,
                                             (triple_connection_size, triple_connection_size))
    readyToSaveThree = pasteCellIntoBackground(threeExtendedToSaveDim, backgroundImage)

    # blur
    return cv2.blur(np.asarray(readyToSaveThree), (3, 3))


def makeSingleton(imagePath, backgroundsPath, resSize=100):
    extension = ".png"
    rotation = random.randrange(4) * 90
    image = Image.open(imagePath).rotate(rotation)

    # add background
    backNumber = random.randrange(len(os.listdir(backgroundsPath)))
    backgroundPath = backgroundsPath + str(backNumber) + extension
    backgroundImage = Image.open(backgroundPath)

    # crop background
    cropped = backgroundImage.crop((0, 0, resSize, resSize))

    # paste
    pasted = pasteCellIntoBackground(image, cropped)

    return cv2.blur(np.asarray(pasted), (3, 3))


def generateNameListsForDirectory(directoryPath):
    namesForTriple = []
    namesForDouble = []
    namesForSingle = []

    allFilenames = os.listdir(directoryPath)

    while len(namesForSingle) < 10:
        filenameIndex = random.randrange(0, len(allFilenames) - 1)
        namesForSingle.append(allFilenames[filenameIndex])

    while len(namesForDouble) < 11:
        firstFilenameInd = random.randrange(0, len(allFilenames) - 1)
        secondFilenameInd = random.randrange(0, len(allFilenames) - 1)
        namesForDouble.append((allFilenames[firstFilenameInd], allFilenames[secondFilenameInd]))

    while len(namesForTriple) < 40:
        firstFilenameInd = random.randrange(0, len(allFilenames) - 1)
        secondFilenameInd = random.randrange(0, len(allFilenames) - 1)
        thirdFilenameInd = random.randrange(0, len(allFilenames) - 1)
        namesForTriple.append(
            (allFilenames[firstFilenameInd], allFilenames[secondFilenameInd], allFilenames[thirdFilenameInd]))

    return namesForSingle, namesForDouble, namesForTriple


def makeConnectionsForCluster(clusterNum, nextCellSaveNumber):
    backgroundsPath = "../backgrounds_by_hand/"
    morphedCellsPath = "../morphing/autoimagemorph-master/erosion_results/"

    negativePathSuffix = "negative/"
    positivePathSuffix = "positive/"
    savePathForSingle = "results/1/"
    savePathForDouble = "results/2/"
    savePathForTriple = "results/3/"
    fileExtension = ".png"

    exist = os.path.exists(savePathForSingle)
    if not exist:
        os.makedirs(savePathForSingle)
    exist = os.path.exists(savePathForDouble)
    if not exist:
        os.makedirs(savePathForDouble)
    exist = os.path.exists(savePathForTriple)
    if not exist:
        os.makedirs(savePathForTriple)

    backForClusterPath = backgroundsPath + str(clusterNum) + "/"

    # neg names
    # negDirPath = morphedCellsPath + str(clusterNum) + "/" + negativePathSuffix
    # negSingleNames, negDoubleNames, negTripleNames = generateNameListsForDirectory(negDirPath)

    # # # neg single
    # for negName in negSingleNames:
    #     try:
    #         blured = makeSingleton(negDirPath + negName, backForClusterPath)
    #         if blured is not None:
    #             filename = str(clusterNum) + "_" + str(nextCellSaveNumber) + fileExtension
    #             rotation = random.randrange(4) * 90
    #             Image.fromarray(blured).rotate(rotation).save(savePathForSingle + filename)
    #             nextCellSaveNumber += 1
    #     except Exception as e:
    #         print(str(e))
    #         print()
    #         print()

    # # neg double
    # for negNames in negDoubleNames:
    #     try:
    #         bluredDouble = connect(negDirPath + negNames[0], negDirPath + negNames[1], backForClusterPath, None)
    #         if bluredDouble is not None:
    #             filename = str(clusterNum) + "_" + str(nextCellSaveNumber) + fileExtension
    #             rotation = random.randrange(4) * 90
    #             Image.fromarray(bluredDouble).rotate(rotation).save(savePathForDouble + filename)
    #             nextCellSaveNumber += 1
    #     except Exception as e:
    #         print(str(e))
    #         print()
    #         print()
    #
    # # neg triple
    # for negNamesTriple in negTripleNames:
    #     try:
    #         bluredTriple = connect(negDirPath + negNamesTriple[0], negDirPath + negNamesTriple[1], backForClusterPath,
    #                                negDirPath + negNamesTriple[2])
    #         if bluredTriple is not None:
    #             filename = str(clusterNum) + "_" + str(nextCellSaveNumber) + fileExtension
    #             rotation = random.randrange(4) * 90
    #             Image.fromarray(bluredTriple).rotate(rotation).save(savePathForTriple + filename)
    #             nextCellSaveNumber += 1
    #     except Exception as e:
    #         print(str(e))
    #         print()
    #         print()

    # # pos names
    posDirPath = morphedCellsPath + str(clusterNum) + "/" + positivePathSuffix
    posSingleNames, posDoubleNames, posTripleNames = generateNameListsForDirectory(posDirPath)
    # #
    # # # pos single
    # for posName in posSingleNames:
    #     try:
    #         blured = makeSingleton(posDirPath + posName, backForClusterPath)
    #         if blured is not None:
    #             filename = str(clusterNum) + "_" + str(nextCellSaveNumber) + fileExtension
    #             rotation = random.randrange(4) * 90
    #             Image.fromarray(blured).rotate(rotation).save(savePathForSingle + filename)
    #             nextCellSaveNumber += 1
    #     except Exception as e:
    #         print(str(e))
    #         print()
    #         print()
    #
    # # # pos double
    # for posNames in posDoubleNames:
    #     try:
    #         bluredDouble = connect(posDirPath + posNames[0], posDirPath + posNames[1], backForClusterPath, None)
    #         if bluredDouble is not None:
    #             filename = str(clusterNum) + "_" + str(nextCellSaveNumber) + fileExtension
    #             rotation = random.randrange(4) * 90
    #             Image.fromarray(bluredDouble).rotate(rotation).save(savePathForDouble + filename)
    #             nextCellSaveNumber += 1
    #     except Exception as e:
    #         print(str(e))
    #         print()
    #         print()
    #
    # # pos triple
    for posNamesTriple in posTripleNames:
        try:
            bluredTriple = connect(posDirPath + posNamesTriple[0], posDirPath + posNamesTriple[1], backForClusterPath,
                                   posDirPath + posNamesTriple[2])
            if bluredTriple is not None:
                filename = str(clusterNum) + "_" + str(nextCellSaveNumber) + fileExtension
                rotation = random.randrange(4) * 90
                Image.fromarray(bluredTriple).rotate(rotation).save(savePathForTriple + filename)
                nextCellSaveNumber += 1
        except Exception as e:
            print(str(e))
            print()
            print()

        # bluredTriple = connect(posDirPath + posNamesTriple[0], posDirPath + posNamesTriple[1], backForClusterPath,
        #                        posDirPath + posNamesTriple[2])
        # filename = str(clusterNum) + "_" + str(nextCellSaveNumber) + fileExtension
        # rotation = random.randrange(4) * 90
        # Image.fromarray(bluredTriple).rotate(rotation).save(savePathForTriple + filename)
        # nextCellSaveNumber += 1

    return nextCellSaveNumber


if __name__ == "__main__":
    nextCellSaveNum = 1

    # cluster 0
    nextCellSaveNum = makeConnectionsForCluster(0, nextCellSaveNum)
    # # cluster 1
    # nextCellSaveNum = makeConnectionsForCluster(1, nextCellSaveNum)
    # # cluster 2
    # nextCellSaveNum = makeConnectionsForCluster(2, nextCellSaveNum)
    # # cluster 3
    # nextCellSaveNum = makeConnectionsForCluster(3, nextCellSaveNum)
