import cv2
import numpy as np

blackColor = 0
whiteColor = 255


def deleteNotConnectedAndFillInside(otsuImage, deleteIfMidLost=True):
    midX = int(otsuImage.shape[0] / 2)
    midY = int(otsuImage.shape[1] / 2)
    des = cv2.bitwise_not(otsuImage)
    contours, _ = cv2.findContours(des, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
    if deleteIfMidLost:
        isMidFilled = False
        for cnt in contours:
            des = np.zeros(otsuImage.shape, dtype=np.uint8)
            cv2.drawContours(des, [cnt], 0, 255, -1)
            if des[midX][midY] == 255:
                isMidFilled = True
                break
        result = cv2.bitwise_not(des) if isMidFilled else None
    else:
        biggest = max(contours, key=lambda c: c.size)
        des = np.zeros(otsuImage.shape, dtype=np.uint8)
        cv2.drawContours(des, [biggest], 0, 255, -1)
        result = cv2.bitwise_not(des)
    return result


def smoothEdges(otsuImage):
    median = cv2.medianBlur(otsuImage, 5)
    _, otsuImage = cv2.threshold(median, 120, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    return otsuImage


def thresholdImage(grayImage):
    _, otsuImage = cv2.threshold(grayImage, 120, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    return otsuImage
















