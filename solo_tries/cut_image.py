import json as jsonLib

from PIL import Image
from numpy import asarray

from Utils import adjustCropIndexesWithBorder, saveImage, imageFromBaseAndOtsu, openImageWithBorder, findCellCenter, \
    deleteExtraLinesByRows
from find_separated_cells import find_separated_cells
from tresholding import *


def getSavePaths(labelId: str, savePathPrefix):
    croppedPath = None
    grayPath = None
    otsuPath = None
    separatedPath = None
    finalImagePath = None
    prefix = "croppedImages/" + savePathPrefix
    if str(labelId) == "1":
        croppedPath = prefix + "pos/"
        otsuPath = prefix + "pos_otsu/"
        grayPath = prefix + "pos_gray/"
        separatedPath = prefix + "pos_separated/"

        croppedPath = prefix + "pos_final/"
        finalImagePath = prefix + "pos_final/"
    elif str(labelId) == "2":
        croppedPath = prefix + "neg/"
        otsuPath = prefix + "neg_otsu/"
        grayPath = prefix + "neg_gray/"
        separatedPath = prefix + "neg_separated/"

        croppedPath = prefix + "neg_final/"
        finalImagePath = prefix + "neg_final/"
    elif str(labelId) == "3":
        croppedPath = prefix + "til/"
        otsuPath = prefix + "til_otsu/"
        grayPath = prefix + "til_gray/"
        separatedPath = prefix + "til_separated/"

        croppedPath = prefix + "til_final/"
        finalImagePath = prefix + "til_final/"
    return croppedPath, grayPath, otsuPath, separatedPath, finalImagePath


def cropImages(filePath, filenameNoExtension, savePathPrefix=''):
    image_path = filePath + filenameNoExtension + '.jpg'
    json_path = filePath + filenameNoExtension + '.json'

    jsonFile = open(json_path)
    json = jsonLib.load(jsonFile)

    image = openImageWithBorder(image_path)

    for i in find_separated_cells(json):
        croppedImage = image.crop(adjustCropIndexesWithBorder(i.x, i.y))
        filename = filenameNoExtension + "_x" + str(i.x) + "_y" + str(i.y) + "_label" + str(i.kind) + ".jpg"

        croppedPath, grayPath, otsuPath, separatedPath, finalImagePath = getSavePaths(str(i.kind), savePathPrefix)

        # cropped image
        saveImage(croppedImage, croppedPath, filename)
        saveImage(croppedImage, croppedPath, filenameNoExtension + "_x" + str(i.x) + "_y" + str(i.y) + "_label" + str(i.kind) + "_1" + ".jpg")

        # gray image
        # grayImage = cv2.cvtColor(cv2.imread(croppedPath + filename), cv2.COLOR_BGR2GRAY)
        grayImage = cv2.cvtColor(cv2.imread(croppedPath + filenameNoExtension + "_x" + str(i.x) + "_y" + str(i.y) + "_label" + str(i.kind) + "_1" + ".jpg"), cv2.COLOR_BGR2GRAY)
        saveImage(Image.fromarray(grayImage), grayPath, filename)

        # thresholding
        otsuImage = thresholdImage(grayImage)
        saveImage(Image.fromarray(otsuImage), otsuPath, filename)

        # cutting and smoothing
        onlyMiddleCell = deleteNotConnectedAndFillInside(otsuImage)
        if onlyMiddleCell is None:
            continue
        smoothedEdges = smoothEdges(onlyMiddleCell)
        saveImage(Image.fromarray(smoothedEdges), separatedPath, filename)

        # applying original colors
        croppedAsNumpy = np.array(croppedImage)
        coloredOtsu = imageFromBaseAndOtsu(croppedAsNumpy, smoothedEdges)

        # if coloredOtsu is not None:
        #     saveImage(Image.fromarray(coloredOtsu), finalImagePath, filename)

        # deleting connected parts of different cells
        cx, cy = findCellCenter(coloredOtsu)
        if cx is not None:
            deleteExtraLinesByRows(smoothedEdges, cx, cy)
            smoothedEdges2 = deleteNotConnectedAndFillInside(smoothedEdges, deleteIfMidLost=False)

            coloredOtsuRotated = imageFromBaseAndOtsu(asarray(Image.fromarray(croppedAsNumpy).rotate(90)),
                                                      asarray(Image.fromarray(smoothedEdges2).rotate(90)))
            rotatedImage = asarray(Image.fromarray(smoothedEdges2).rotate(90))
            rotatedMidX, rotatedMidY = findCellCenter(coloredOtsuRotated)
            if rotatedMidX is not None and rotatedMidY is not None:
                deleteExtraLinesByRows(rotatedImage, rotatedMidX, rotatedMidY)
                backRotation = asarray(Image.fromarray(rotatedImage).rotate(270))

                backRotationSmooth = deleteNotConnectedAndFillInside(backRotation, deleteIfMidLost=False)

                coloredOtsu = imageFromBaseAndOtsu(croppedAsNumpy, backRotationSmooth)
        #
        #     mid color
            # makeRedAtPosition(coloredOtsu, rotatedMidX, rotatedMidY)
            # makeRedMid(coloredOtsu)

                if coloredOtsu is not None:
                    saveImage(Image.fromarray(coloredOtsu), finalImagePath, filenameNoExtension + "_x" + str(i.x) + "_y" + str(i.y) + "_label" + str(i.kind) + "_2" + ".jpg")
