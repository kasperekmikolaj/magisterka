from Utils import cellsDistance


class Cell:

    def __init__(self, x: int, y: int, kind: str):
        self.x = x
        self.y = y
        self.kind = kind

    def isClose(self, other):
        isClose = False
        if abs(self.x - other.x) < cellsDistance:
            if abs(self.y - other.y) < cellsDistance:
                isClose = True
        return isClose

    def __eq__(self, other):
        if self.x == other.x:
            if self.y == other.y:
                return True
        return False


def find_separated_cells(json):
    cells = []
    res = []
    for elem in json:
        cells.append(Cell(elem["x"], elem["y"], elem["label_id"]))
    for i in range(len(cells)):
        current = cells[i]
        closeToCurrent = list(filter(lambda other: current.isClose(other) and not current == other, cells))
        if len(closeToCurrent) == 0:
            res.append(current)
    return res

