from typing import List

import cv2
from PIL import Image

from cut_image import saveImage


def morph(sourceImage, destinationImage, iterations=4) -> List:
    morphed = []
    for i in range(iterations):
        changePercentage = float((i+1) / iterations)
        print(changePercentage)
        newImage = cv2.addWeighted(sourceImage, 1 - changePercentage, destinationImage, changePercentage, 0)
        morphed.append(newImage)
    return morphed


if __name__ == '__main__':
    firstImagePath = "alpha_blending/0.jpg"
    secondImagePath = "alpha_blending/1.jpg"

    srcImage = cv2.imread(firstImagePath)
    destImage = cv2.imread(secondImagePath)

    resList = morph(srcImage, destImage)
    for index, image in enumerate(resList):
        cv2.imwrite("alpha_blending/" + "res_" + str(index) + '.jpg', image)
