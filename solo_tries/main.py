import glob

import numpy as np
from PIL import Image

from Utils import makeRedAtPosition, saveImage
from cut_image import cropImages
import json as jsonLib


def markCellMid():
    json_path = path + filenameNoExtension + '.json'
    jsonFile = open(json_path)
    json = jsonLib.load(jsonFile)
    image_path = path + filenameNoExtension + '.jpg'

    image = Image.open(image_path)
    imageCopy = np.array(image)
    for i in json:
        color = None
        if i["label_id"] == 1:
            color = [255, 0, 0]
        elif i["label_id"] == 2:
            color = [0, 0, 255]
        else:
            color = [0, 255, 0]
        makeRedAtPosition(imageCopy, i["y"], i["x"], color)
    saveImage(Image.fromarray(imageCopy), "croppedImages/Train/cellMidShown/", filenameNoExtension + ".jpg")


if __name__ == '__main__':

    # filenameBase = 'data/Train/p1_0299_6'
    # cropImages(filenameBase, "p1_0299_6")

    path = "Z:/OneDrive - Politechnika Wroclawska/studia/praca_magisterska/python/cell_image_cutter/data/Train/"
    # path = "/data/cell_mid_marked_train/"

    for filename in glob.glob(path + '*.jpg'):
        # savePathPrefix = "Train/"
        filenameNoExtension = (filename.split("\\")[-1]).split(".")[0]
        # cropImages(path, filenameNoExtension, savePathPrefix)

        try:
            markCellMid()
        except Exception:
            pass

    # testPath = "Z:/OneDrive - Politechnika Wroclawska/studia/praca_magisterska/python/cell_image_cutter/data/Test/"
    # for filename in glob.glob(testPath + '*.jpg'):
    #     savePathPrefix = "Test/"
    #     filenameNoExtension = filename.split(".")[0]
    #     cropImages(path, filenameNoExtension, savePathPrefix)
