import os

import cv2
import numpy as np
from PIL import Image


def blackBack(image):
    cop = np.copy(image)
    for rowi in range(image.shape[0]):
        for coli in range(image.shape[1]):
            # if image[rowi][coli] == 255:
            if (image[rowi][coli] == [255, 255, 255]).all():
                cop[rowi][coli] = 0, 0, 0
    return cop


def goColor(gray, color):
    cop = np.copy(color)
    for rowi in range(gray.shape[0]):
        for coli in range(gray.shape[1]):
            if len(gray.shape) == 2:
                if gray[rowi][coli] == 255:
                    cop[rowi][coli] = 255, 255, 255
            else:
                if (gray[rowi][coli] != [255, 255, 255]).all():
                    cop[rowi][coli] = 255, 255, 255
    return cop


def createBinary(image):
    blackColor = 0
    whiteColor = 255
    asArray = np.asarray(image)
    new_im = np.zeros(image.shape, "uint8")

    for rowIndex in range(asArray.shape[0]):
        for colIndex in range(asArray.shape[1]):
            # if (asArray[rowIndex][colIndex] != [255, 255, 255]).all():
            if asArray[rowIndex][colIndex] != 255:
                new_im[rowIndex][colIndex] = whiteColor
            else:
                new_im[rowIndex][colIndex] = blackColor
    return new_im


def erodeImage(imagePath, iterations=1):
    img = cv2.imread(imagePath, 0)
    imgColor = cv2.imread(imagePath)
    imgColor = cv2.cvtColor(imgColor, cv2.COLOR_BGR2RGB)
    binary = createBinary(img)
    kernel = np.ones((5, 5), np.uint8)
    img_erosion = cv2.erode(binary, kernel, iterations=iterations)
    return goColor(cv2.cvtColor(img_erosion, cv2.COLOR_BGR2RGB), imgColor)


if __name__ == "__main__":

    # Taking a matrix of size 5 as the kernel
    kernel = np.ones((5, 5), np.uint8)

    negativePathSuffix = "negative/"
    positivePathSuffix = "positive/"

    # morphingDir = "../morphing/autoimagemorph-master/"
    morphingDir = ""
    currentResultsPath = morphingDir + "results/"
    # newResults = morphingDir + "thresholded_results_no_erosion/"
    erosionResults = morphingDir + "erosion_results/"

    whiteColor = (255, 255, 255)

    # create save directories
    savingPaths = []
    for i in range(4):
        # savingPaths.append(newResults + str(i) + "/" + negativePathSuffix)
        # savingPaths.append(newResults + str(i) + "/" + positivePathSuffix)
        savingPaths.append(erosionResults + str(i) + "/" + negativePathSuffix)
        savingPaths.append(erosionResults + str(i) + "/" + positivePathSuffix)
    for path in savingPaths:
        exist = os.path.exists(path)
        if not exist:
            os.makedirs(path)

    # morphing thresholding
    # for i in range(4):
    #     if i == 2:
    #         continue
    #
    #     negPath = currentResultsPath + str(i) + "/" + negativePathSuffix
    #     negFiles = os.listdir(negPath)
    #     for negName in negFiles:
    #         img = np.asarray(Image.open(negPath + negName))
    #         # thresholding morphing result
    #         for row_index in range(img.shape[0]):
    #             for col_index in range(img.shape[1]):
    #                 elem = img[row_index][col_index]
    #                 suma = int(elem[0]) + int(elem[1]) + int(elem[2])
    #                 if suma > 735:
    #                     img[row_index][col_index] = whiteColor
    #         # Image.fromarray(img).save(newResults + str(i) + "/" + negativePathSuffix + negName)
    #
    #         eroded = erodeImage(negPath + negName)
    #         # thresholding erosion result
    #         for row_index in range(eroded.shape[0]):
    #             for col_index in range(eroded.shape[1]):
    #                 elem = eroded[row_index][col_index]
    #                 suma = int(elem[0]) + int(elem[1]) + int(elem[2])
    #                 if suma > 735:
    #                     eroded[row_index][col_index] = whiteColor
    #         # saving eroded
    #         Image.fromarray(eroded).save(erosionResults + str(i) + "/" + negativePathSuffix + negName)

    for i in range(4):
        if i == 2:
            continue

        posPath = currentResultsPath + str(i) + "/" + positivePathSuffix
        posFiles = os.listdir(posPath)
        for posName in posFiles:
            img = np.asarray(Image.open(posPath + posName))
            for row_index in range(img.shape[0]):
                for col_index in range(img.shape[1]):
                    elem = img[row_index][col_index]
                    suma = int(elem[0]) + int(elem[1]) + int(elem[2])
                    if suma > 735:
                        img[row_index][col_index] = whiteColor
            # Image.fromarray(img).save(newResults + str(i) + "/" + positivePathSuffix + posName)

            eroded = erodeImage(posPath + posName)
            for row_index in range(eroded.shape[0]):
                for col_index in range(eroded.shape[1]):
                    elem = eroded[row_index][col_index]
                    suma = int(elem[0]) + int(elem[1]) + int(elem[2])
                    if suma > 735:
                        eroded[row_index][col_index] = whiteColor
            Image.fromarray(eroded).save(erosionResults + str(i) + "/" + positivePathSuffix + posName)
