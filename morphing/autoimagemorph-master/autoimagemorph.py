import copy
import os
import random

import cv2
import imageio
import numpy as np
import time
from PIL import Image, ImageDraw
from matplotlib import pyplot as plt
from scipy.interpolate import RectBivariateSpline
from scipy.ndimage import median_filter
from scipy.spatial import Delaunay
import cv2 as cv


def goColor(gray, color):
    cop = np.copy(color)
    for rowi in range(gray.shape[0]):
        for coli in range(gray.shape[1]):
            if len(gray.shape) == 2:
                if gray[rowi][coli] == 255:
                    cop[rowi][coli] = 255, 255, 255
            else:
                if (gray[rowi][coli] != [255, 255, 255]).all():
                    cop[rowi][coli] = 255, 255, 255
    return cop


def createBinary(image):
    blackColor = 0
    whiteColor = 255
    asArray = np.asarray(image)
    new_im = np.zeros(image.shape, "uint8")

    for rowIndex in range(asArray.shape[0]):
        for colIndex in range(asArray.shape[1]):
            # if (asArray[rowIndex][colIndex] != [255, 255, 255]).all():
            if asArray[rowIndex][colIndex] != 255:
                new_im[rowIndex][colIndex] = whiteColor
            else:
                new_im[rowIndex][colIndex] = blackColor
    return new_im


def erodeImage(imagePath, iterations=1):
    img = cv2.imread(imagePath, 0)
    imgColor = cv2.imread(imagePath)
    imgColor = cv2.cvtColor(imgColor, cv2.COLOR_BGR2RGB)
    binary = createBinary(img)
    kernel = np.ones((5, 5), np.uint8)
    img_erosion = cv2.erode(binary, kernel, iterations=iterations)
    return goColor(cv2.cvtColor(img_erosion, cv2.COLOR_BGR2RGB), imgColor)


def deletePointsFromDifferentHalves(midRowIndex, midColIndex, firstList, secondList, maxDistance=10):
    firstResult = []
    secondResult = []
    for index in range(len(firstList)):
        l_col, l_row = firstList[index]
        r_col, r_row = secondList[index]
        # for x
        x_differentHalves = True if (l_row - midRowIndex) * (r_row - midRowIndex) < 0 else False
        if not (x_differentHalves and abs(l_row - r_row) > maxDistance):
            # for y
            y_differentHalves = True if (l_col - midColIndex) * (r_col - midColIndex) < 0 else False
            if not (y_differentHalves and abs(l_col - r_col) > maxDistance):
                firstResult.append(firstList[index])
                secondResult.append(secondList[index])
    return np.asarray(firstResult), np.asarray(secondResult)


def loadTriangles(limg, rimg, pointPerQuarter) -> tuple:
    leftTriList = []
    rightTriList = []

    lrlists = autofeaturepoints(limg, rimg, pointPerQuarter)

    leftArray = np.array(lrlists[0], np.float64)
    rightArray = np.array(lrlists[1], np.float64)

    leftArray, rightArray = deletePointsFromDifferentHalves(int(limg.shape[0] / 2), int(limg.shape[1] / 2), leftArray,
                                                            rightArray)

    delaunayTri = Delaunay(leftArray)

    leftNP = leftArray[delaunayTri.simplices]
    rightNP = rightArray[delaunayTri.simplices]

    for x, y in zip(leftNP, rightNP):
        leftTriList.append(Triangle(x))
        rightTriList.append(Triangle(y))

    return leftTriList, rightTriList


class Triangle:
    def __init__(self, vertices):
        if isinstance(vertices, np.ndarray) == 0:
            raise ValueError("Input argument is not of type np.array.")
        if vertices.shape != (3, 2):
            raise ValueError("Input argument does not have the expected dimensions.")
        if vertices.dtype != np.float64:
            raise ValueError("Input argument is not of type float64.")
        self.vertices = vertices
        self.minX = int(self.vertices[:, 0].min())
        self.maxX = int(self.vertices[:, 0].max())
        self.minY = int(self.vertices[:, 1].min())
        self.maxY = int(self.vertices[:, 1].max())

    def getPoints(self):
        width = round(max(self.vertices[:, 0]) + 2)
        height = round(max(self.vertices[:, 1]) + 2)
        mask = Image.new('P', (width, height), 0)
        ImageDraw.Draw(mask).polygon(tuple(map(tuple, self.vertices)), outline=255, fill=255)
        coordArray = np.transpose(np.nonzero(mask))
        return coordArray


class Morpher:
    def __init__(self, leftImage, leftTriangles, rightImage, rightTriangles):
        if type(leftImage) != np.ndarray:
            raise TypeError('Input leftImage is not an np.ndarray')
        if leftImage.dtype != np.uint8:
            raise TypeError('Input leftImage is not of type np.uint8')
        if type(rightImage) != np.ndarray:
            raise TypeError('Input rightImage is not an np.ndarray')
        if rightImage.dtype != np.uint8:
            raise TypeError('Input rightImage is not of type np.uint8')
        if type(leftTriangles) != list:
            raise TypeError('Input leftTriangles is not of type List')
        for j in leftTriangles:
            if isinstance(j, Triangle) == 0:
                raise TypeError('Element of input leftTriangles is not of Class Triangle')
        if type(rightTriangles) != list:
            raise TypeError('Input leftTriangles is not of type List')
        for k in rightTriangles:
            if isinstance(k, Triangle) == 0:
                raise TypeError('Element of input rightTriangles is not of Class Triangle')
        self.leftImage = copy.deepcopy(leftImage)
        self.newLeftImage = copy.deepcopy(leftImage)
        self.leftTriangles = leftTriangles  # Not of type np.uint8
        self.rightImage = copy.deepcopy(rightImage)
        self.newRightImage = copy.deepcopy(rightImage)
        self.rightTriangles = rightTriangles  # Not of type np.uint8

    def getImageAtAlpha(self, alpha, smoothMode):
        for leftTriangle, rightTriangle in zip(self.leftTriangles, self.rightTriangles):
            self.interpolatePoints(leftTriangle, rightTriangle, alpha)
        return ((1 - alpha) * self.newLeftImage + alpha * self.newRightImage).astype(np.uint8)

    def interpolatePoints(self, leftTriangle, rightTriangle, alpha):
        targetTriangle = Triangle(leftTriangle.vertices + (rightTriangle.vertices - leftTriangle.vertices) * alpha)
        targetVertices = targetTriangle.vertices.reshape(6, 1)
        tempLeftMatrix = np.array([[leftTriangle.vertices[0][0], leftTriangle.vertices[0][1], 1, 0, 0, 0],
                                   [0, 0, 0, leftTriangle.vertices[0][0], leftTriangle.vertices[0][1], 1],
                                   [leftTriangle.vertices[1][0], leftTriangle.vertices[1][1], 1, 0, 0, 0],
                                   [0, 0, 0, leftTriangle.vertices[1][0], leftTriangle.vertices[1][1], 1],
                                   [leftTriangle.vertices[2][0], leftTriangle.vertices[2][1], 1, 0, 0, 0],
                                   [0, 0, 0, leftTriangle.vertices[2][0], leftTriangle.vertices[2][1], 1]])
        tempRightMatrix = np.array([[rightTriangle.vertices[0][0], rightTriangle.vertices[0][1], 1, 0, 0, 0],
                                    [0, 0, 0, rightTriangle.vertices[0][0], rightTriangle.vertices[0][1], 1],
                                    [rightTriangle.vertices[1][0], rightTriangle.vertices[1][1], 1, 0, 0, 0],
                                    [0, 0, 0, rightTriangle.vertices[1][0], rightTriangle.vertices[1][1], 1],
                                    [rightTriangle.vertices[2][0], rightTriangle.vertices[2][1], 1, 0, 0, 0],
                                    [0, 0, 0, rightTriangle.vertices[2][0], rightTriangle.vertices[2][1], 1]])
        try:
            lefth = np.linalg.solve(tempLeftMatrix, targetVertices)
            righth = np.linalg.solve(tempRightMatrix, targetVertices)
            leftH = np.array(
                [[lefth[0][0], lefth[1][0], lefth[2][0]], [lefth[3][0], lefth[4][0], lefth[5][0]], [0, 0, 1]])
            rightH = np.array(
                [[righth[0][0], righth[1][0], righth[2][0]], [righth[3][0], righth[4][0], righth[5][0]], [0, 0, 1]])
            leftinvH = np.linalg.inv(leftH)
            rightinvH = np.linalg.inv(rightH)
            targetPoints = targetTriangle.getPoints()

            # Credit to https://github.com/zhifeichen097/Image-Morphing for the following code block that I've adapted. Exceptional work on discovering
            # RectBivariateSpline's .ev() method! I noticed the method but didn't think much of it at the time due to the website's poor documentation..
            xp, yp = np.transpose(targetPoints)
            leftXValues = leftinvH[1, 1] * xp + leftinvH[1, 0] * yp + leftinvH[1, 2]
            leftYValues = leftinvH[0, 1] * xp + leftinvH[0, 0] * yp + leftinvH[0, 2]
            leftXParam = np.arange(np.amin(leftTriangle.vertices[:, 1]), np.amax(leftTriangle.vertices[:, 1]), 1)
            leftYParam = np.arange(np.amin(leftTriangle.vertices[:, 0]), np.amax(leftTriangle.vertices[:, 0]), 1)
            leftImageValues = self.leftImage[int(leftXParam[0]):int(leftXParam[-1] + 1),
                              int(leftYParam[0]):int(leftYParam[-1] + 1)]

            rightXValues = rightinvH[1, 1] * xp + rightinvH[1, 0] * yp + rightinvH[1, 2]
            rightYValues = rightinvH[0, 1] * xp + rightinvH[0, 0] * yp + rightinvH[0, 2]
            rightXParam = np.arange(np.amin(rightTriangle.vertices[:, 1]), np.amax(rightTriangle.vertices[:, 1]), 1)
            rightYParam = np.arange(np.amin(rightTriangle.vertices[:, 0]), np.amax(rightTriangle.vertices[:, 0]), 1)
            rightImageValues = self.rightImage[int(rightXParam[0]):int(rightXParam[-1] + 1),
                               int(rightYParam[0]):int(rightYParam[-1] + 1)]

            self.newLeftImage[xp, yp] = RectBivariateSpline(leftXParam, leftYParam, leftImageValues, kx=1, ky=1).ev(
                leftXValues, leftYValues)
            self.newRightImage[xp, yp] = RectBivariateSpline(rightXParam, rightYParam, rightImageValues, kx=1, ky=1).ev(
                rightXValues, rightYValues)
        except:
            return


########################################################################################################
def findMainDirectionPoints(black_white_image):
    blackColor = 0

    start_row = None
    end_row = None
    for row_index in range(black_white_image.shape[0]):
        if start_row is None and blackColor in black_white_image[row_index]:
            start_row = row_index
        elif start_row is not None and end_row is None and blackColor not in black_white_image[row_index]:
            end_row = row_index - 1
            break
    if end_row is None:
        end_row = black_white_image.shape[0] - 1

    start_col = None
    end_col = None
    for col_index in range(black_white_image.shape[1]):
        if start_col is None and blackColor in black_white_image[:, col_index]:
            start_col = col_index
        elif start_col is not None and end_col is None and blackColor not in black_white_image[:, col_index]:
            end_col = col_index - 1
            break
    if end_col is None:
        end_col = black_white_image.shape[1] - 1

    mid_row_index = int((end_row - start_row) / 2 + start_row)
    l_r_list = findPointsForRow(black_white_image, mid_row_index)
    left = l_r_list[0]
    right = l_r_list[1]

    mid_col_index = int((end_col - start_col) / 2 + start_col)
    t_b_list = findPointsForColumn(black_white_image, mid_col_index)
    top = t_b_list[0]
    bot = t_b_list[1]

    return left, top, right, bot, mid_row_index, mid_col_index


def findPointsForRow(black_white_image, row_index):
    whiteColor = 255
    left = None
    right = None
    for col_index in range(black_white_image.shape[1]):
        if left is None and black_white_image[row_index][col_index] != whiteColor:
            left = col_index
        elif left is not None and black_white_image[row_index][col_index] == whiteColor:
            # check if not single pixel
            if col_index + 1 < black_white_image.shape[1] and black_white_image[row_index][col_index + 1] == whiteColor:
                right = col_index - 1
                break
    if right is None:
        endIndex = black_white_image.shape[1] - 1
        if black_white_image[row_index][endIndex] != whiteColor:
            right = endIndex
        else:
            return None
    return [left, right]


def findPointsForColumn(black_white_image, column_index):
    whiteColor = 255
    top = None
    bot = None
    for row_index in range(black_white_image.shape[0]):
        if top is None and black_white_image[row_index][column_index] != whiteColor:
            top = row_index
        elif top is not None and black_white_image[row_index][column_index] == whiteColor:
            # check if not single pixel
            if row_index + 1 < black_white_image.shape[0] and black_white_image[row_index + 1][
                column_index] == whiteColor:
                bot = row_index - 1
                break
    if bot is None:
        endIndex = black_white_image.shape[0] - 1
        if black_white_image[endIndex][column_index] != whiteColor:
            bot = endIndex
        else:
            return None
    return [top, bot]


# Automatic feature points
def autofeaturepoints(leimg, riimg, pointPerQuarter=2):
    result = [[], []]

    # looping twice - for every image
    for idx, img in enumerate([leimg, riimg]):

        # add the 4 corners to result
        result[idx] = [[0, 0], [(img.shape[1] - 1), 0], [0, (img.shape[0] - 1)],
                       [(img.shape[1] - 1), (img.shape[0] - 1)]]

        # find first 4 points
        grayScale = np.asarray(Image.fromarray(img).convert('L'))
        _, black_white_image = cv.threshold(grayScale, 235, 255, cv.THRESH_BINARY)
        left, top, right, bot, mid_row_index, mid_col_index = findMainDirectionPoints(black_white_image)

        # first val is column index second is row index
        result[idx].append([left, mid_row_index])
        result[idx].append([right, mid_row_index])
        result[idx].append([mid_col_index, top])
        result[idx].append([mid_col_index, bot])

        result[idx].append([mid_col_index, mid_row_index])

        # add left and right points
        row_indexes = []
        dif = int((bot - top) / 2 / (pointPerQuarter + 1))
        for i in range(1, pointPerQuarter + 1):
            row_indexes.append(mid_row_index + dif * i)
            row_indexes.append(mid_row_index - dif * i)
        for row_index in row_indexes:
            l_r_list = findPointsForRow(black_white_image, row_index)
            if l_r_list is not None:
                l = l_r_list[0]
                r = l_r_list[1]
                result[idx].append([l, row_index])
                result[idx].append([r, row_index])

        # add top and bot points
        col_indexes = []
        dif = int((right - left) / 2 / (pointPerQuarter + 1))
        for i in range(1, pointPerQuarter + 1):
            col_indexes.append(mid_col_index + dif * i)
            col_indexes.append(mid_col_index - dif * i)
        for col_index in col_indexes:
            t_b_list = findPointsForColumn(black_white_image, col_index)
            if t_b_list is not None:
                t = t_b_list[0]
                b = t_b_list[1]
                result[idx].append([col_index, t])
                result[idx].append([col_index, b])

        # print points
        # colors = [(255, 0, 0), (252, 186, 3), (69, 252, 3), (3, 252, 206), (3, 136, 252), (144, 0, 255), (247, 0, 255), (255, 221, 0), (240, 115, 117)]
        # imgcopy = np.copy(img)
        # for index, point in enumerate(result[idx]):
        #     imgcopy[int(point[1])][int(point[0])] = colors[0]
        # img1 = np.pad(img, ((1, 1), (1, 1), (0, 0)), constant_values=0)
        # imgcopy1 = np.pad(imgcopy, ((1, 1), (1, 1), (0, 0)), constant_values=0)
        # fig, ax = plt.subplots(1, 3)
        # fig.set_size_inches(18.5, 10.5)
        # ax[0].imshow(img1, interpolation='none')
        # ax[0].set_title("input", fontsize=50)
        # ax[0].axis('off')
        # ax[1].imshow(imgcopy1)
        # ax[1].set_title("with red", fontsize=50)
        # ax[1].axis('off')
        # ax[2].imshow(black_white_image)
        # ax[2].set_title("b_w", fontsize=50)
        # ax[2].axis('off')
        # plt.show()

    return result


def initmorph(startimgpath, endimgpath, pointPerQuarter):
    leftImageRaw = cv2.imread(startimgpath)
    rightImageRaw = cv2.imread(endimgpath)

    leftImageARR = np.asarray(leftImageRaw)
    rightImageARR = np.asarray(rightImageRaw)

    leftImageARR = makeWhite(leftImageARR)
    rightImageARR = makeWhite(rightImageARR)

    filename = "tests/results/" + 'a' + ".png"
    cv2.imwrite(filename, leftImageARR)
    filename = "tests/results/" + 'z' + ".png"
    cv2.imwrite(filename, rightImageARR)

    # autofeaturepoints() is called in loadTriangles()
    triangleTuple = loadTriangles(leftImageRaw, rightImageRaw, pointPerQuarter)

    # Morpher objects for color layers BGR
    morphers = [
        Morpher(leftImageARR[:, :, 0], triangleTuple[0], rightImageARR[:, :, 0], triangleTuple[1]),
        Morpher(leftImageARR[:, :, 1], triangleTuple[0], rightImageARR[:, :, 1], triangleTuple[1]),
        Morpher(leftImageARR[:, :, 2], triangleTuple[0], rightImageARR[:, :, 2], triangleTuple[1])
    ]

    return morphers


def morphprocess(mphs, framerate, outimgprefix, smoothing):
    global framecnt

    # frame_0 is the starting image, so framecnt = _1
    framecnt = framecnt + 1
    lastFrameNumber = framerate - 1

    # loop generate morph frames and save
    for i in range(1, framerate):

        timerstart = time.time()
        alfa = i / framerate

        # image calculation and smoothing BGR
        if smoothing > 0:
            outimage = np.dstack([
                np.array(median_filter(mphs[0].getImageAtAlpha(alfa, True), smoothing)),
                np.array(median_filter(mphs[1].getImageAtAlpha(alfa, True), smoothing)),
                np.array(median_filter(mphs[2].getImageAtAlpha(alfa, True), smoothing)),
            ])
        else:
            outimage = np.dstack([
                np.array(mphs[0].getImageAtAlpha(alfa, True)),
                np.array(mphs[1].getImageAtAlpha(alfa, True)),
                np.array(mphs[2].getImageAtAlpha(alfa, True)),
            ])

        # # save frames (without src and dest images)
        # if framecnt != 1 and framecnt != lastFrameNumber:
        #     filename = "results/" + outimgprefix + str(framecnt) + ".png"
        #     cv2.imwrite(filename, outimage)
        # framecnt = framecnt + 1


        for row_index in range(outimage.shape[0]):
            for col_index in range(outimage.shape[1]):
                elem = outimage[row_index][col_index]
                suma = int(elem[0]) + int(elem[1]) + int(elem[2])
                if suma > 735:
                    outimage[row_index][col_index] = (255,255,255)\



        filename = "tests/results/temp/" + outimgprefix + str(framecnt) + ".png"
        exist = os.path.exists("tests/results/temp/")
        if not exist:
            os.makedirs("tests/results/temp/")
        cv2.imwrite(filename, outimage)

        eroded = erodeImage(filename)
        for row_index in range(eroded.shape[0]):
            for col_index in range(eroded.shape[1]):
                elem = eroded[row_index][col_index]
                suma = int(elem[0]) + int(elem[1]) + int(elem[2])
                if suma > 735:
                    eroded[row_index][col_index] = (255, 255, 255)

        filename = "tests/results/" + outimgprefix + str(framecnt) + ".png"
        # outimage = makeBlack(outimage)
        cv2.imwrite(filename, outimage)

        framecnt = framecnt + 1


def batchmorph(imgs, framerate, outimgprefix, smoothing, pointPerQuarter):
    global framecnt
    framecnt = 0
    for idx in range(len(imgs) - 1):
        morphprocess(
            initmorph(imgs[idx], imgs[idx + 1], pointPerQuarter),
            framerate, outimgprefix, smoothing
        )


def makegif():
    filenames = [filename for filename in os.listdir("tests/results")]
    filenames.sort(key=lambda filename: int(filename.replace('f', '').replace(".png", '')))
    img_array = []
    for filename in filenames:
        # img = cv2.imread("results/" + filename)
        img = cv2.imread("tests/results/" + filename)
        img_array.append(img)

    for frame in reversed(img_array):
        img_array.append(frame)
    imageio.mimsave('tests/video.gif', img_array, duration=float(100 / 1000))


def makeWhite(imageAsArray):
    for col in imageAsArray:
        for i in range(len(col)):
            if (col[i] == (0,0,0)).all():
                col[i] = (255,255,255)
    return imageAsArray


def makeBlack(imageAsArray):
    for col in imageAsArray:
        for i in range(len(col)):
            if (col[i] == (255,255,255)).all():
                col[i] = (0,0,0)
    return imageAsArray


if __name__ == "__main__":
    # "-outprefix", default="", required=True, help="REQUIRED output filename prefix, -outprefix f  will write/overwrite f1.png f2.png ...")
    # "-framerate", type=int, default=mframerate, help="Frames to render between each keyframe +1, for example -framerate 30 will render 29 frames between -inframes ['f0.png','f30.png'] (default: %(default)s)")
    # "-smoothing", type=int, default=msmoothing, help="median_filter smoothing/blur to remove image artifacts, for example -smoothing 2 will blur lightly. (default: %(default)s)")
    # pointPerQuarter = 2
    # framerate = 8
    # smoothing = 0
    # dataPath = "../../data/selected/"
    # negativePath = "negative/"
    # positivePath = "positive/"
    #
    # # # create save directories
    # savingPaths = []
    # for i in range(4):
    #     savingPaths.append("results/" + str(i) + "/" + negativePath)
    #     savingPaths.append("results/" + str(i) + "/" + positivePath)
    # for path in savingPaths:
    #     exist = os.path.exists(path)
    #     if not exist:
    #         os.makedirs(path)
    #
    # # run morphing
    # positive_counter = 0
    # negative_counter = 0
    #
    # while (positive_counter + negative_counter) * (framerate - 2) < 100:
    #     for clusterNumber in range(4):
    #         # super low on data
    #         if clusterNumber == 2:
    #             continue
    #
    #         negativePairs = []
    #         positivePairs = []
    #         negDirPath = dataPath + str(clusterNumber) + "/" + negativePath
    #         posDirPath = dataPath + str(clusterNumber) + "/" + positivePath
    #
    #         imageNamesInNegDirectory = os.listdir(negDirPath)
    #         negRange = len(imageNamesInNegDirectory) // 2 * 2
    #
    #         imageNamesInPosDirectory = os.listdir(posDirPath)
    #         posRange = len(imageNamesInPosDirectory) // 2 * 2
    #
    #         random.shuffle(imageNamesInNegDirectory)
    #         # pair neg
    #         for imageIndex in range(0, negRange, 2):
    #             firstPath = dataPath + str(clusterNumber) + "/" + negativePath + imageNamesInNegDirectory[imageIndex]
    #             secondPath = dataPath + str(clusterNumber) + "/" + negativePath + imageNamesInNegDirectory[
    #                 imageIndex + 1]
    #             negativePairs.append([firstPath, secondPath])
    #         # morph and save neg
    #         for negPair in negativePairs:
    #             outprefix = str(clusterNumber) + '/' + negativePath + str(negative_counter) + "_"
    #             batchmorph(negPair, framerate, outprefix, smoothing, pointPerQuarter)
    #             negative_counter += 1
    #         random.shuffle(imageNamesInPosDirectory)
    #         # pair positive
    #         for imageIndex in range(0, posRange, 2):
    #             firstPath = dataPath + str(clusterNumber) + "/" + positivePath + imageNamesInPosDirectory[imageIndex]
    #             secondPath = dataPath + str(clusterNumber) + "/" + positivePath + imageNamesInPosDirectory[
    #                 imageIndex + 1]
    #             positivePairs.append([firstPath, secondPath])
    #         # morph and save positive
    #         for posPair in positivePairs:
    #             outprefix = str(clusterNumber) + '/' + positivePath + str(positive_counter) + "_"
    #             batchmorph(posPair, framerate, outprefix, smoothing, pointPerQuarter)
    #             positive_counter += 1

    # # @@@@@@@@ TESTS
    dataset = 15
    pointPerQuarter = 1

    # srcPath = "tests/example_sets/" + str(dataset) + "/src.jpg"
    # destPath = "tests/example_sets/" + str(dataset) + "/dest.jpg"

    srcPath = "tests/temp/" + str(dataset) + "/1.png"
    destPath = "tests/temp/" + str(dataset) + "/2.png"

    imgList = [srcPath, destPath]
    framerate = 8
    outprefix = 'f'
    smoothing = 0

    batchmorph(imgList, framerate, outprefix, smoothing, pointPerQuarter)
    # makegif()
    # # @@@@@@@@ TESTS
