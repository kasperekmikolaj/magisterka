import json as jsonLib
import os
import time

import cv2
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from PIL import Image

from Utils import imageFromBaseAndOtsu, adjustCropIndexes, saveImage, getCellExtremeColPointsFromMid, \
    getCellExtremeRowPointsFromMid, extendImageSize, closest_background
from cellcutter_github import cellcutter
from cellcutter_github.cellcutter import utils

np.set_printoptions(precision=4)

mpl.rcParams['image.cmap'] = 'gray'


def draw_single_label(data, model, image):
    '''
    data: cellcutter.dataset object
    model: a tf NN model
    image: a 2D array for size reference
    '''
    coords = data.labeled_cords
    preds = tf.sigmoid(model(data.patches)).numpy().squeeze()
    d0, d1 = preds.shape[-2:]
    max_prob = np.zeros(image.shape, dtype=np.float32)
    # find the max prob at each pixel
    # we have to go through patchs sequentially to have a predictable execution order
    for coord, pred in zip(coords, preds):
        c0, c1, _ = list(coord)
        max_prob[c0:c0 + d0, c1:c1 + d1] = np.maximum(max_prob[c0:c0 + d0, c1:c1 + d1], pred)
    label = 1
    images = []
    # now remove any pred output that is not the max
    for coord, pred in zip(coords, preds):
        c0, c1, cell_label = list(coord)
        pred[pred < max_prob[c0:c0 + d0, c1:c1 + d1]] = 0
        next_image = np.zeros(image.shape, dtype=np.float32)
        next_image[c0:c0 + d0, c1:c1 + d1] = np.maximum(image[c0:c0 + d0, c1:c1 + d1], (pred > 0.5) * label)
        images.append((next_image, c1, c0, cell_label))
    return images


def deleteNotConnectedAndFillInside(otsuImage, deleteIfMidLost=True):
    midX = int(otsuImage.shape[0] / 2)
    midY = int(otsuImage.shape[1] / 2)
    des = np.copy(otsuImage)
    contours, _ = cv2.findContours(des, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
    if deleteIfMidLost:
        isMidFilled = False
        for cnt in contours:
            des = np.zeros(otsuImage.shape, dtype=np.uint8)
            cv2.drawContours(des, [cnt], 0, 255, -1)
            if des[midX][midY] == 255:
                isMidFilled = True
                break
        result = des if isMidFilled else None
    else:
        biggest = max(contours, key=lambda c: c.size)
        des = np.zeros(otsuImage.shape, dtype=np.uint8)
        cv2.drawContours(des, [biggest], 0, 255, -1)
        result = des
    return result


def cut_and_save(filePrefix, training_epochs, startNumber=0):
    # file and mask
    test_path = "../data/Train/" + filePrefix + ".jpg"
    jsonFile = jsonLib.load(open("../data/Train/" + filePrefix + ".json"))

    input_image = Image.open(test_path)

    # assign to cluster based on avg image color
    color_avg = np.average(np.asarray(input_image), axis=(0, 1)).astype("int")
    colorCluster = closest_background(color_avg)

    gray_image = np.asarray(input_image.convert('L'))
    input_image = np.asarray(input_image)
    mask = cellcutter.utils.graph_cut(gray_image, prior=0.15, max_weight=10, sigma=0.01)

    # markers - white fields are totally ignored in segmentation process
    test_markers = [(elem["y"], elem['x']) for elem in jsonFile]
    labeled_cords = [(elem["y"], elem['x'], elem['label_id']) for elem in jsonFile]

    # model training
    print("training started for file: " + filePrefix)
    dataset = cellcutter.Dataset(gray_image, test_markers, labeled_cords,
                                 mask_img=mask)  # actually need the inverse of the mask
    # dataset = cellcutter.Dataset(gray_image, test_markers, labeled_cords)  # no mask test
    start = time.time()
    model = cellcutter.UNet4(bn=True)
    cellcutter.train_self_supervised(dataset, model, n_epochs=training_epochs)
    print('Training time: %f' % (time.time() - start))

    # get single cell mask
    single_cell_images_with_cords = draw_single_label(dataset, model, np.zeros_like(gray_image, dtype=int))

    # crop, color, filter, save
    i = startNumber
    for singleCellImage, x, y, cell_label in single_cell_images_with_cords:
        # crop image
        crop_indexes = adjustCropIndexes(x, y)
        cropped_original = np.asarray(Image.fromarray(input_image).crop(crop_indexes))
        cropped_binary = np.asarray(Image.fromarray(singleCellImage).crop(crop_indexes))

        # delete single pixels
        noNoise = np.copy(cropped_binary)
        noNoise = cv2.medianBlur(noNoise, 5)

        # delete not connected parts
        noNoise = deleteNotConnectedAndFillInside((noNoise * 255).astype('int'), True)
        if noNoise is None:
            continue

        # omit if mid is not white
        midX = int(noNoise.shape[0] / 2)
        midY = int(noNoise.shape[1] / 2)
        if noNoise[midX][midY] != 255:
            continue

        # apply colors
        result = imageFromBaseAndOtsu(cropped_original, cropped_binary, valToColor=0)

        # delete not connected - part 2
        # MASK DIFFERENT FROM ORIGINAL NOW
        left, right = getCellExtremeColPointsFromMid(result)
        top, bot = getCellExtremeRowPointsFromMid(result)
        resultHeight = result.shape[1]
        resultWidth = result.shape[0]
        resultCropped = Image.fromarray(result).crop((left, top, right, bot))
        result = extendImageSize(resultCropped, (resultWidth, resultHeight))

        # save colored
        cell_label_str = "positive" if cell_label == 1 else "negative" if cell_label == 2 else None
        if cell_label_str is None:
            continue
        filename = str(i) + ".png"
        # maskPath = "cutter_results/" + filePrefix + "/" + cell_label_str + "_mask/"
        filePath = "cutter_results/colorCluster_" + str(colorCluster) + "/" + cell_label_str + "/"
        # saveBinaryImage(noNoise, maskPath, filename)
        saveImage(result, filePath, filename)

        i += 1
    return i


# 1st one to run
if __name__ == "__main__":

    training_epochs = 15
    dataPath = "../data/Train/"
    nextSaveNumber = 0
    # run for whole dir
    for (root, dirs, files) in os.walk(dataPath):
        files.sort()
        for f in files:
            if '.jpg' in f:
                filePref = f[: -4]
                try:
                    nextSaveNumber = cut_and_save(filePref, training_epochs, nextSaveNumber)
                except Exception as e:
                    print(e)
                    print()
                    print()
                    print()

    # # run for single image
    # filePrefix = "p2_0235_6"
    # cut_and_save(filePrefix, training_epochs)


    # plt.show()
