import json as jsonLib
import time

import cv2
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from PIL import Image
from skimage.color import label2rgb

from cellcutter_github import cellcutter
from cellcutter_github.cellcutter import utils
from Utils import imageFromBaseAndOtsu, adjustCropIndexes, saveImage, saveBinaryImage

np.set_printoptions(precision=4)

mpl.rcParams['image.cmap'] = 'gray'


def draw_single_label(data, model, image):
    '''
    data: cellcutter.dataset object
    model: a tf NN model
    image: a 2D array for size reference
    '''
    coords = data.labeled_cords
    preds = tf.sigmoid(model(data.patches)).numpy().squeeze()
    d0, d1 = preds.shape[-2:]
    max_prob = np.zeros(image.shape, dtype=np.float32)
    # find the max prob at each pixel
    # we have to go through patchs sequentially to have a predictable execution order
    for coord, pred in zip(coords, preds):
        c0, c1, _ = list(coord)
        max_prob[c0:c0 + d0, c1:c1 + d1] = np.maximum(max_prob[c0:c0 + d0, c1:c1 + d1], pred)
    label = 1
    images = []
    # now remove any pred output that is not the max
    for coord, pred in zip(coords, preds):
        c0, c1, cell_label = list(coord)
        pred[pred < max_prob[c0:c0 + d0, c1:c1 + d1]] = 0
        next_image = np.zeros(image.shape, dtype=np.float32)
        next_image[c0:c0 + d0, c1:c1 + d1] = np.maximum(image[c0:c0 + d0, c1:c1 + d1], (pred > 0.5) * label)
        images.append((next_image, c1, c0, cell_label))
    return images


def deleteNotConnectedAndFillInside(otsuImage, deleteIfMidLost=True):
    midX = int(otsuImage.shape[0] / 2)
    midY = int(otsuImage.shape[1] / 2)
    des = np.copy(otsuImage)
    contours, _ = cv2.findContours(des, cv2.RETR_CCOMP, cv2.CHAIN_APPROX_SIMPLE)
    if deleteIfMidLost:
        isMidFilled = False
        for cnt in contours:
            des = np.zeros(otsuImage.shape, dtype=np.uint8)
            cv2.drawContours(des, [cnt], 0, 255, -1)
            if des[midX][midY] == 255:
                isMidFilled = True
                break
        result = des if isMidFilled else None
    else:
        biggest = max(contours, key=lambda c: c.size)
        des = np.zeros(otsuImage.shape, dtype=np.uint8)
        cv2.drawContours(des, [biggest], 0, 255, -1)
        result = des
    return result

# the same script as instance_segmentation.py but with printed steps
if __name__ == "__main__":
    training_epochs = 15

    # file and mask
    filePrefix = "p1_0309_7"
    test_path = "../data/Train/" + filePrefix + ".jpg"
    jsonFile = jsonLib.load(open("../data/Train/" + filePrefix + ".json"))

    input_image = Image.open(test_path)
    gray_image = np.asarray(input_image.convert('L'))
    input_image = np.asarray(input_image)
    mask = cellcutter.utils.graph_cut(gray_image, prior=0.15, max_weight=10, sigma=0.01)
    #
    fig, ax = plt.subplots(1, 2)
    fig.set_size_inches(18.5, 10.5)
    ax[0].imshow(gray_image)
    ax[0].set_title("input", fontsize=50)
    ax[0].axis('off')
    ax[1].imshow(~mask)
    ax[1].set_title("mask", fontsize=50)
    ax[1].axis('off')
    plt.show()

    # markers - white fields are totally ignored in segmentation process
    test_markers = [(elem["y"], elem['x']) for elem in jsonFile]
    labeled_cords = [(elem["y"], elem['x'], elem['label_id']) for elem in jsonFile]
    #
    fig, ax = plt.subplots(1, 2)
    ax[0].imshow(gray_image)
    ax[0].set_title("input", fontsize=18)
    ax[0].axis('off')
    ax[1].imshow(gray_image)
    ax[1].set_title("markers", fontsize=18)
    ax[1].axis('off')
    plt.show()

    for y, x in test_markers:
        c = plt.Circle((x, y), 15, color='gray', linewidth=2, fill=False)
        ax[1].add_patch(c)
    fig.set_size_inches(18.5, 10.5)

    # check blobs on mask
    fig, ax = plt.subplots(1, 3)
    ax[0].imshow(gray_image)
    ax[0].set_title("input", fontsize=18)
    ax[0].axis('off')
    ax[1].imshow(mask)
    ax[1].set_title("mask", fontsize=18)
    ax[1].axis('off')
    ax[2].imshow(mask)
    ax[2].set_title("mask+blobs", fontsize=18)
    ax[2].axis('off')
    plt.show()

    for y, x in test_markers:
        c = plt.Circle((x, y), 15, color='gray', linewidth=2, fill=False)
        ax[2].add_patch(c)
    fig.set_size_inches(25, 15)

    # model training
    print("training started")
    # dataset = cellcutter.Dataset(input_image, test_markers, labeled_cords, mask_img=mask)  # actually need the inverse of the mask
    # dataset = cellcutter.Dataset(gray_image, test_markers, labeled_cords, mask_img=mask)  # actually need the inverse of the mask

    dataset = cellcutter.Dataset(gray_image, test_markers, labeled_cords)  # no mask test
    start = time.time()
    model = cellcutter.UNet4(bn=True)
    cellcutter.train_self_supervised(dataset, model, n_epochs=training_epochs)
    print('Training time: %f' % (time.time() - start))

    # results
    label = cellcutter.utils.draw_label(dataset, model, np.zeros_like(gray_image, dtype=int))
    rgb = label2rgb(label, bg_label=0)
    border = cellcutter.utils.draw_border(dataset, model, np.zeros_like(gray_image, dtype=int))
    #
    fig, ax = plt.subplots(1, 3)
    ax[0].imshow(gray_image)
    ax[0].set_title("input", fontsize=18)
    ax[0].axis('off')
    ax[1].imshow(rgb)
    ax[1].set_title("segmentation", fontsize=18)
    ax[1].axis('off')
    ax[2].imshow(border)
    ax[2].set_title("borders", fontsize=18)
    ax[2].axis('off')
    fig.set_size_inches(25, 15)
    plt.show()


    # get single cell mask
    single_cell_images_with_cords = draw_single_label(dataset, model, np.zeros_like(gray_image, dtype=int))
    #
    fig, ax = plt.subplots(1, 2)
    ax[0].imshow(gray_image)
    ax[0].axis('off')
    ax[1].imshow(single_cell_images_with_cords[3][0])
    ax[1].axis('off')
    plt.show()


    # crop, color, filter, save
    i = 0
    for singleCellImage, x, y, cell_label in single_cell_images_with_cords:
        # crop image
        crop_indexes = adjustCropIndexes(x, y)
        cropped_original = np.asarray(Image.fromarray(input_image).crop(crop_indexes))
        cropped_binary = np.asarray(Image.fromarray(singleCellImage).crop(crop_indexes))

        # delete single pixels
        noNoise = np.copy(cropped_binary)
        noNoise = cv2.medianBlur(noNoise, 5)

        # delete not connected parts
        noNoise = deleteNotConnectedAndFillInside(noNoise, True)

        # apply colors
        result = imageFromBaseAndOtsu(cropped_original, cropped_binary, valToColor=0)

        # save mask and colored
        # cell_label_str = "positive" if cell_label == 1 else "negative" if cell_label == 2 else "til"
        # filename = filePrefix + "_" + str(i) + ".png"
        # maskPath = "cutter_results/" + filePrefix + "/" + cell_label_str + "_mask/"
        # filePath = "cutter_results/" + filePrefix + "/" + cell_label_str + "/"
        # saveBinaryImage(noNoise, maskPath, filename)
        # saveImage(Image.fromarray(result), filePath, filename)

        i += 1
